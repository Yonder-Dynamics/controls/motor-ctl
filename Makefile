CC=gcc
TARGET=build
DEPS=pid.h
OBJ=pid.o

default: main

main: init
	$(CC) -c pid.c -o $(TARGET)/$(OBJ)
	$(CC) -o $(TARGET)/pid $(TARGET)/$(OBJ)

init:
	mkdir -p $(TARGET)

clean: init
	rm -rf $(TARGET)
