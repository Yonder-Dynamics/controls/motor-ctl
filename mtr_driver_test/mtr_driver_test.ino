
Servo m1;

void setup() {
  /*pinMode(ENB, OUTPUT);*/
  /*pinMode(IN2, OUTPUT);*/
  /*pinMode(IN3, OUTPUT);*/
  m1.attach(7);
  Serial.begin(115200);
}

void loop() {
  for (int i=0; i<2; i++) {
    for (int j=-MAX_SPEED; j<-MIN_SPEED; j+=10) {
      move(j);
      delay(300);
    }
    for (int j=MIN_SPEED; j<MAX_SPEED; j+=10) {
      move(j);
      delay(300);
    }
  }
  move(0);
  delay(2000);
  move(0);
  delay(2000);
}
