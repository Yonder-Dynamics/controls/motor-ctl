#include <Servo.h>

const unsigned int HASH_PRIME = 59359;

// Doesn't register until 104, 87
const int MIN_ANGLE = 40;
const int MAX_ANGLE = 150;
const int MAX_SPEED_CHANGE = 2;

const int UNCONNECTED = 11;
const int MTR_FR = 8;
const int MTR_MR = UNCONNECTED;
const int MTR_BR = 3;

const int MTR_FL = 4;
const int MTR_ML = UNCONNECTED;
const int MTR_BL = 6;

typedef struct {
  unsigned char pin;
  unsigned char max_speed;
  int goal_speed;
  int current_speed;
  Servo handle;
} Motor;

typedef struct {
  unsigned char num_speeds;
  unsigned char target_system; // ie drive, arm, drill
  unsigned int hash;
  int * speeds;
} Message;

unsigned int hash_msg(Message * msg) {
  unsigned int sum = 0;
  for (int i=0; i<msg->num_speeds; i++) {
    sum += msg->speeds[i] * HASH_PRIME;
  }
  return sum + msg->target_system * HASH_PRIME;
}

void init_motors(Motor * mtrs, int num_mtrs) {
  for (int i=0; i<num_mtrs; i++) {
    mtrs[i].handle = Servo();
    mtrs[i].handle.attach(mtrs[i].pin);
    mtrs[i].goal_speed = 0;
    mtrs[i].current_speed = 0;
  }
}

void set_goal_speeds(Motor * mtrs, int num_mtrs, int *val) {
  for (int i=0; i<num_mtrs; i++) {
    mtrs[i].goal_speed = min(mtrs[i].max_speed,
                             max(val[i], -mtrs[i].max_speed));
  }
}

void update_system(Motor * mtrs, int num_mtrs) {
  for (int i=0; i<num_mtrs; i++) {
    int difference = mtrs[i].goal_speed - mtrs[i].current_speed;
    int actual_movement = min(MAX_SPEED_CHANGE, abs(difference));
    if(difference != 0){
      mtrs[i].current_speed += actual_movement * difference/abs(difference);
    }
    int current_angle = int(float(mtrs[i].current_speed)
                / 255. * (MAX_ANGLE-MIN_ANGLE)/2
                + (MAX_ANGLE-MIN_ANGLE)/2
                + MIN_ANGLE); 
    mtrs[i].handle.write(current_angle);
  }
}

void move(Motor * mtrs, int num_mtrs, int * val) {
  for (int i=0; i<num_mtrs; i++) {
    val[i] = min(mtrs[i].max_speed, max(val[i], -mtrs[i].max_speed));
    int current_angle = int(float(val[i])
                / 255. * (MAX_ANGLE-MIN_ANGLE)/2
                + (MAX_ANGLE-MIN_ANGLE)/2
                + MIN_ANGLE); 
    mtrs[i].handle.write(current_angle);
  }
}

void stop(Motor * mtrs, int num_mtrs) {
  int mtr_vals[num_mtrs];
  for (int i=0; i<num_mtrs; i++) {
    mtr_vals[i] = 0;
  }
  move(mtrs, num_mtrs, mtr_vals);
}

void calibrate(Motor * mtrs, int num_mtrs) {
  for (int i=0; i<2; i++) {
    for (int j=-255; j<255; j+=10) {
      int mtr_vals[num_mtrs];
      for (int i=0; i<num_mtrs; i++) {
        mtr_vals[i] = j;
      }
      move(mtrs, num_mtrs, mtr_vals);
      delay(300);
    }
  }
  stop(mtrs, num_mtrs);
  delay(2000);
  stop(mtrs, num_mtrs);
  delay(2000);
}
