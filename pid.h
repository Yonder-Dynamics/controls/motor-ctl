#ifndef PID
#define PID

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// pid controller object
struct pid_controller {

    double kP;
    double kI;
    double kD;
    double kF;
    double tolerance;
    double settling_time;

    int inverted;
    int abs_set_point;
    int no_oscillation;
    double min_target;
    double max_target;
    double min_output;
    double max_output;

    double prev_time;
    double prev_error;
    double total_error;
    double settling_start_time;
    double set_point;
    double set_point_sign;
    double input;
    double output;

    double p_term;
    double i_term;
    double d_term;
    double f_term;

};

typedef struct pid_controller * pid_ptr;

pid_ptr pid_create(pid_ptr pid, double kP, double kI, double kD, double kF, double tolerance, double settling_time);

void set_target_range(pid_ptr pid, double min_target, double max_target);

void set_output_range(pid_ptr pid, double min_output, double max_output);

void set_target(pid_ptr pid, double target);

void set_abs_point(pid_ptr pid, int abs);

void set_inverted(pid_ptr pid, int inverted);

void compute(pid_ptr pid);

int on_target(pid_ptr pid);

#endif