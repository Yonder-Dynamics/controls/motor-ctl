import math

test_inputs = [
        # x, y
        ((1, 0), "turn right"), 
        ((0, 1), "go straight"),
        ((0, -1), "go back"),
        ((-1, 0), "turn left"),
        ((-1, 1), "turn up left"),
        ((1, 1), "turn up right"),
        ((-1, -1), "turn down left"),
        ((1, -1), "turn down right"),
]

eps = 0.1

def make_mtr_cmd(x, y):
    vel_mag = 1
    norm = math.sqrt(y**2 + x**2)
    # norm = abs(y) + abs(x)
    x = x/norm
    y = y/norm
    # angle = (math.atan2(y, x) - math.pi/2)
    # Mod by pi
    # angle = angle + math.pi if angle <= -math.pi+eps else angle
    # angle = angle if angle < math.pi/2+eps else math.pi/2 - angle
    # angle = angle if angle > -math.pi/2-eps else -math.pi/2 - angle
    # ratio = angle / math.pi * 4
    # print("Ratio: {}".format(ratio))
    # left_speed = vel_mag*(1-ratio)*255
    # right_speed = vel_mag*(1+ratio)*255
    print(x, y)
    left_speed = (y - x)* 255/norm
    right_speed = (y + x)* 255/norm
    print(left_speed, right_speed)
    speeds = [int(right_speed)]* 3 + [int(left_speed)]* 3
    return speeds

for ((x, y), desc) in test_inputs:
    print(make_mtr_cmd(x, y), desc)
