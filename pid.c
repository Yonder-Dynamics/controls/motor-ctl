#include "pid.h"

pid_ptr pid_create(pid_ptr pid, double kP, double kI, double kD, double kF, double tolerance, double settling_time)
{
    pid->kP = kP;
    pid->kI = kI;
    pid->kD = kD;
    pid->kF = kF;
    pid->tolerance = tolerance;
    pid->settling_time = settling_time;
    return pid;
}

void set_target_range(pid_ptr pid, double min_target, double max_target)
{
    pid->min_target = min_target;
    pid->max_target = max_target;
}

void set_output_range(pid_ptr pid, double min_output, double max_output)
{
    pid->min_output = min_output;
    pid->max_output = max_output;
}

double sign(double x)
{
    return (x > 0.0) - (x < 0.0);
}

void set_abs_point(pid_ptr pid, int abs) {
    pid->abs_set_point = abs;
}

void set_inverted(pid_ptr pid, int inverted) {
    pid->inverted = inverted;
}

void set_target(pid_ptr pid, double target)
{
    if (!pid->abs_set_point)
    {
        pid->set_point = pid->input + target;
        pid->prev_error = target;
    }
    else
    {
        pid->set_point = target;
        pid->prev_error = pid->set_point - pid->input;
    }
    pid->set_point_sign = sign(pid->prev_error);
    if (pid->max_target > pid->min_target)
    {
        if (pid->set_point > pid->max_target)
        {
            pid->set_point = pid->max_target;
        }
        else if (pid->set_point < pid->min_target)
        {
            pid->set_point = pid->min_target;
        }
    }

    // time in nano seconds
    pid->prev_time = 0;
    if (pid->inverted)
    {
        pid->prev_error = -pid->prev_error;
    }
    pid->total_error = 0.0;
    // time in nano seconds
    pid->settling_start_time = 0;
}

int on_target(pid_ptr pid)
{
    int is_on_target = 0;

    if (pid->no_oscillation)
    {
        if (pid->prev_error * pid->set_point_sign <= pid->tolerance)
        {
            is_on_target = 1;
        }
    }
    else if (fabs(pid->prev_error) > pid->tolerance)
    {
        // get current time in nano seconds
        pid->settling_start_time = 0;
    }
    // change -1 to current time in nano seconds
    else if (-1 >= pid->settling_start_time + pid->settling_time)
    {
        is_on_target = 1;
    }

    return is_on_target;
}

void compute(pid_ptr pid)
{
    double curr_time = 0;
    double delta = pid->prev_time - curr_time;
    pid->prev_time = curr_time;
    double error = pid->set_point - pid->input;
    if (pid->inverted)
    {
        error = -error;
    }

    if (pid->kI != 0.0)
    {
        double potential_gain = (pid->total_error + error * delta) * pid->kI;
        if (potential_gain >= pid->max_output)
        {
            pid->total_error = pid->max_output / pid->kI;
        }
        else if (potential_gain > pid->min_output)
        {
            pid->total_error += error * delta;
        }
        else
        {
            pid->total_error = pid->min_output / pid->kI;
        }
    }

    pid->p_term = pid->kP * error;
    pid->i_term = pid->kI * pid->total_error;
    pid->d_term = delta > 0.0 ? pid->kD * (error - pid->prev_error) / delta : 0.0;
    pid->f_term = pid->kF * pid->set_point;
    pid->output = pid->p_term + pid->i_term + pid->d_term + pid->f_term;

    pid->prev_error = error;
    if (pid->output > pid->max_output)
    {
        pid->output = pid->max_output;
    }
    else if (pid->output < pid->min_output)
    {
        pid->output = pid->min_output;
    }
}

int main(int argc, char const argv[])
{
    printf("Testing PID\n");
    struct pid_controller pidctrl;
    pid_ptr pid;
    pid = pid_create(&pidctrl, 0, 0, 0, 0, 0, 0);
    // pid->input should be set to the motor encoder info like left encoder position
    /* Ex - for 2 motor drive train

    int prev_left_pos = 0;
    int prev_right_pos = 0;
    double stall_start_time = 0;

    set_target(pid);
    // time in nano seconds
    stall_start_time = time();

    while ( !on_target(pid) ) {
        int curr_left_pos = get_curr_pos(left_motor);
        int curr_right_pos = get_curr_pos(right_motor);
        compute(pid);
        set_motor_power(left_motor, pid->output);
        set_motor_power(right_motor, pid->output);
        // time in nano seconds
        double curr_time = time();
        if ( curr_left_pos != prev_left_pos || curr_right_pos != prev_right_pos ) {
            stall_start_time = curr_time;
            prev_left_pos = curr_left_pos;
            prev_right_pos = curr_right_pos;
        } else if ( curr_time > stall_start_time + 0.15 ) {
            break;
        }
    }
    */
    return 0;
}