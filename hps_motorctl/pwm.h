#ifndef pwmh
#define pwmh
#include "gpio.h"

#include <unistd.h>
#include <stdio.h>

#include <stdlib.h>
#include <stdbool.h>

void run_pwm();

typedef struct pwmpin
{
    pin p_pos, p_neg;
    char pwmval;
    bool dir;
} pwmpin;

extern volatile size_t num_pwm_pins;
extern volatile pwmpin *pwm_pins;
#endif