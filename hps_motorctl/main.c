
#include "pwm.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "hwlib.h"
#include "socal/socal.h"
#include "socal/hps.h"
#include "socal/alt_gpio.h"
#include <pthread.h>
#include "gpio.h"

bool inrange(char c, char low, char h)
{
	return c >= low && c <= h;
}

int main(int argc, char **argv) //warning: INCREDIBLY HACKY!
//pipe desired input to stdin
//format: "255,255,255,255\n"
{

	if (init_gpio() != 0)
	{
		return 1;
	}
	/*
	int num_pwm = 4;

	volatile pwmpin pins[4] = {{//use this to communcate across threads
															{pin_from_idx(0),
															 pin_from_idx(1),
															 0,
															 false},
															{pin_from_idx(2),
															 pin_from_idx(3),
															 0,
															 false},
															{pin_from_idx(4),
															 pin_from_idx(5),
															 0,
															 false},
															{pin_from_idx(6),
															 pin_from_idx(7),
															 0,
															 false}}};

	for (int i = 0; i < 8; i++)
	{
		set_IO_dir(pin_from_idx(i), true);
	}*/

	set_IO_dir(pin_from_idx(0), true);
	while (true)
	{
		set_out_value(pin_from_idx(0), true);
		sleep(1);
		set_out_value(pin_from_idx(0), false);
		sleep(1);
	}
	/*
	pwm_pins = pins;
	num_pwm_pins = num_pwm;

	pthread_t pwm_thread;
	//pthread_create(&pwm_thread, NULL, run_pwm, NULL);

	
	while (true)
	{
		char input[255];
		if (fgets(input, stdin, 255) == NULL)
		{
			break;
		}
		int char_idx = 0;
		int pwm_idx = 0;
		int num_start = 0, num_end = 0, last_num_read;
		while (char_idx < 255 && pwm_idx < 4 && input[char_idx] != '\0')
		{
			if (inrange(input[char_idx], '0', '9'))
			{
				num_end++;
			}
			else
			{
				last_num_read = strtol(input + num_start, NULL, 10);
				num_start = ++num_end;
				setDuty(pwm_idx, last_num_read);
				pwm_idx++;
			}
			char_idx++;
		}
	}
	*/

	// clean up our memory mapping and exit
	//pthread_cancel(pwm_thread);
	return close_gpio();
}
