#include "pwm.h"
#include "gpio.h"
#include <stdio.h>
#define _POSIX_C_SOURCE 199309L
#include <time.h>

#define FREQ 1000000
#define PER 1000000000 / FREQ
#define SUBCYC_PER PER / 256

volatile size_t num_pwm_pins;
volatile pwmpin *pwm_pins;

void setDuty(size_t index, char duty)
{
    pwm_pins[index].pwmval = duty;
}

long get_nsec()
{
    long ns;  // Milliseconds
    time_t s; // Seconds
    struct timespec spec;

    clock_gettime(CLOCK_REALTIME, &spec); //ignore this, VSCode is just being an MS product.

    ns = spec.tv_nsec; // Convert nanoseconds to milliseconds
    return ns;
}

void run_pwm()
{
    long start = get_nsec();
    while (1)
    {
        for (int i = 0; i < num_pwm_pins; i++)
        {
            long now = get_nsec();
            int diff = (start - now) / SUBCYC_PER;
            if (pwm_pins[i].dir)
            {
                set_out_value(&pwm_pins[i].p_pos, pwm_pins[i].pwmval > diff % 256);
                set_out_value(&pwm_pins[i].p_neg, 0);
            }
            else
            {
                set_out_value(&pwm_pins[i].p_pos, 0);
                set_out_value(&pwm_pins[i].p_neg, pwm_pins[i].pwmval > diff % 256);
            }
        }
    }
}