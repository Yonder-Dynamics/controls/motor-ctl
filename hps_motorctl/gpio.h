#ifndef gpioh
#define gpioh
#include <unistd.h>

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include <stdbool.h>

#define HW_REGS_BASE (ALT_STM_OFST)
#define HW_REGS_SPAN (0x04000000)
#define HW_REGS_MASK (HW_REGS_SPAN - 1)

#define USER_IO_DIR (0x01000000)
#define BIT_LED (0x01000000)
#define BUTTON_MASK (0x02000000)

int init_gpio();
int close_gpio();

typedef struct gpioport
{
    void *read_address;
    void *write_address;
    void *config_address;
} gpioport;

typedef struct pin
{
    gpioport *port;
    uint32_t mask;
} pin;

int set_out_value(pin *, bool);
#endif