#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "hwlib.h"
#include "socal/socal.h"
#include "socal/hps.h"
#include "socal/alt_gpio.h"
#include "gpio.h"

void *virtual_base;
int fd;
uint32_t scan_input;
int i;

gpioport gpio0 = {
    ALT_GPIO0_SWPORTA_DR_ADDR,
    ALT_GPIO0_EXT_PORTA_ADDR,
    ALT_GPIO0_SWPORTA_DDR_ADDR};

gpioport gpio1 = {ALT_GPIO1_SWPORTA_DR_ADDR, ALT_GPIO1_SWPORTA_DDR_ADDR, ALT_GPIO1_EXT_PORTA_ADDR};

gpioport gpio2 = {ALT_GPIO2_SWPORTA_DR_ADDR, ALT_GPIO2_SWPORTA_DDR_ADDR, ALT_GPIO2_EXT_PORTA_ADDR};

pin pin_from_idx(uint32_t idx)
{
    pin p = {
        idx < 29 ? &gpio0 : idx < 58 ? &gpio1 : &gpio2,
        1 << idx % 29};
    return p;
}

void set_IO_dir(pin *p, bool iodir)
{
    uint32_t cfg = alt_read_word((uint32_t *)((p->port)->config_address + (uint32_t)virtual_base));
    if (iodir)
    {
        cfg |= p->mask;
    }
    else
    {
        cfg &= ~(p->mask);
    }
    alt_write_word(
        (uint32_t *)((p->port)->config_address + (uint32_t)virtual_base), cfg);
}

int set_out_value(pin *p, bool value)
{
    uint32_t val = (uint32_t)alt_read_word((uint32_t *)((p->port)->read_address + (uint32_t)virtual_base));
    if (value)
    {
        val |= p->mask;
    }
    else
    {
        val &= ~(p->mask);
    }
    alt_write_word(
        (uint32_t *)((p->port)->read_address + (uint32_t)virtual_base),
        val);
}
bool get_in_value(uint32_t pin_idx)
{
    return 0 != alt_read_word(virtual_base + (((uint32_t)(ALT_GPIO0_SWPORTA_DDR_ADDR) + (pin_idx / 32)) & (uint32_t)(HW_REGS_MASK))) & (0x1 << (pin_idx % 32));
}

int init_gpio()
{
    if ((fd = open("/dev/mem", (O_RDWR | O_SYNC))) == -1)
    {
        printf("ERROR: could not open \"/dev/mem\"...\n");
        return (1);
    }

    alt_setbits_word((virtual_base + ((uint32_t)(ALT_GPIO1_SWPORTA_DDR_ADDR) & (uint32_t)(HW_REGS_MASK))), USER_IO_DIR);

    virtual_base = mmap(NULL, HW_REGS_SPAN, (PROT_READ | PROT_WRITE), MAP_SHARED, fd, HW_REGS_BASE);

    if (virtual_base == MAP_FAILED)
    {
        printf("ERROR: mmap() failed...\n");
        close(fd);
        return (1);
    }
    return 0;
}

int close_gpio()
{
    if (munmap(virtual_base, HW_REGS_SPAN) != 0)
    {
        printf("ERROR: munmap() failed...\n");
        close(fd);
        return (1);
    }
    close(fd);
    return 0;
}